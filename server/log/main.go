package main

import (
	"gitlab.com/bazzz/log/server"
)

func main() {
	logServer := server.NewDefault()
	panic(logServer.Start())
}
