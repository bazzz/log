package datastores

import (
	"database/sql"
	"os"
	"path/filepath"

	_ "github.com/lib/pq" // postgresql
	"gitlab.com/bazzz/log/server"
)

// PostgresqlDataStore is a data store for the log server with Postgresql backend.
type PostgresqlDataStore struct {
	db *sql.DB
}

// NewPostgresqlDataStore is the constructor for PostgresqlDataStore.
func NewPostgresqlDataStore(user, pass, host, name, sslmode string) (*PostgresqlDataStore, error) {
	exe, err := os.Executable()
	if err != nil {
		return nil, err
	}
	applicationName := filepath.Base(exe)
	connStr := "postgres://" + user + ":" + pass + "@" + host + "/" + name + "?sslmode=" + sslmode + "&fallback_application_name=" + applicationName
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	ds := PostgresqlDataStore{
		db: db,
	}
	return &ds, nil
}

// Add adds the message to the data store.
func (p *PostgresqlDataStore) Add(application string, severity int, message string) error {
	query := "INSERT INTO entry(application, severity, message) VALUES($1, $2, $3);"
	_, err := p.db.Exec(query, application, severity, message)
	if err != nil {
		return err
	}
	return nil
}

// ViewRecent returns the log entries of the last 7 days.
func (p *PostgresqlDataStore) ViewRecent() ([]server.Entry, error) {
	query := `SELECT timestamp, application, severity, message FROM entry 
	WHERE timestamp > current_date - interval '7' day
	ORDER BY timestamp DESC;`
	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}
	result := readRows(rows)
	return result, nil
}

// ViewByMinSeverity returns the log entries with severity at least as high as parameter severity.
func (p *PostgresqlDataStore) ViewByMinSeverity(severity int) ([]server.Entry, error) {
	query := `SELECT timestamp, application, severity, message FROM entry 
	WHERE severity >= $1
	ORDER BY timestamp DESC;`
	rows, err := p.db.Query(query, severity)
	if err != nil {
		return nil, err
	}
	result := readRows(rows)
	return result, nil
}

// Applications returns a distinct list of all application names.
func (p *PostgresqlDataStore) Applications() ([]string, error) {
	result := make([]string, 0)
	query := `SELECT DISTINCT application FROM entry;`
	rows, err := p.db.Query(query)
	if err != nil {
		return result, err
	}
	for rows.Next() {
		application := ""
		rows.Scan(&application)
		result = append(result, application)
	}
	return result, nil
}

func readRows(rows *sql.Rows) []server.Entry {
	result := make([]server.Entry, 0)
	for rows.Next() {
		entry := server.Entry{}
		rows.Scan(&entry.Timestamp, &entry.Application, &entry.Severity, &entry.Message)
		result = append(result, entry)
	}
	return result
}
