package server

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
)

// Server is a global log server that accepts log calls from any application on a REST based url.
type Server struct {
	httpServer *http.Server
	store      DataStore
	severities map[string]int
}

// Start activates the log server.
func (s *Server) Start() error {
	return s.httpServer.ListenAndServe()
}

func (s *Server) root(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}

func (s *Server) add(w http.ResponseWriter, r *http.Request) {
	application := r.PostFormValue("application")
	severity, _ := strconv.Atoi(r.PostFormValue("severity"))
	message := r.PostFormValue("message")
	if application == "" || severity < 1 || severity > 4 || message == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err := s.store.Add(application, severity, message)
	if err != nil {
		s.store.Add("log server - could not serve add request:", 3, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func (s *Server) view(w http.ResponseWriter, r *http.Request) {
	var entries []Entry
	var err error
	values := strings.Split(r.URL.Path[len("/view/"):], "/")
	if len(values) == 0 || s.severities[values[0]] == 0 {
		entries, err = s.store.ViewRecent()
	} else {
		entries, err = s.store.ViewByMinSeverity(s.severities[values[0]])
	}
	if err != nil {
		s.store.Add("log server - could not serve view request:", 3, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(entries)
	if err != nil {
		s.store.Add("log server - could not serve applications request:", 3, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func (s *Server) applications(w http.ResponseWriter, r *http.Request) {
	apps, err := s.store.Applications()
	if err != nil {
		s.store.Add("log server - could not serve applications request:", 3, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(apps)
	if err != nil {
		s.store.Add("log server - could not serve applications request:", 3, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

// NewDefault calls New() with { host:"", port:9999, store:nil }.
func NewDefault() *Server {
	return New("", 9999, nil)
}

// New is the constructor for Server listening on host:port with optional Datastore, if store == nil, a default memory store will be used.
func New(host string, port int, store DataStore) *Server {
	server := Server{}

	severities := make(map[string]int)
	severities["info"] = 1
	severities["warning"] = 2
	severities["error"] = 3
	severities["fatal"] = 4
	server.severities = severities

	addr := host + ":" + strconv.Itoa(port)
	handler := http.NewServeMux()
	handler.HandleFunc("/", server.root)
	handler.HandleFunc("/add/", server.add)
	handler.HandleFunc("/view/", server.view)
	handler.HandleFunc("/applications/", server.applications)
	httpServer := &http.Server{
		Addr:    addr,
		Handler: handler,
	}
	server.httpServer = httpServer

	if store == nil {
		store = newMemoryStore()
	}
	server.store = store
	return &server
}
