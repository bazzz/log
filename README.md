# log

Package log provides logger functionality to a file or a central log server based on per app logging. You can use Connect() to connect to a simple log server implementation. The log server has a default memory store but can be provided with other storage backends. An example implementation for postgresql backend is provided.