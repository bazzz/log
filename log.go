package log

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var std = new()

func new() *Logger {
	exe, err := os.Executable()
	if err != nil {
		panic(err)
	}
	appName := filepath.Base(exe)
	logger := Logger{
		applicationName: appName,
		logfilePath:     filepath.Join(filepath.Dir(exe), appName+".log"),
	}
	return &logger
}

// Connect connects to the log server at localhost:9999, if a connection cannot be made it returns false and gracefully starts logging to file instead.
func Connect() bool {
	return std.Connect()
}

// ConnectTo connects to the log server at host:port, if a connection cannot be made it returns false and gracefully starts logging to file instead.
func ConnectTo(host string, port int) bool {
	return std.ConnectTo(host, port)
}

// Print adds a line of text to the log with a severity level 1.
func Print(text ...interface{}) {
	std.log(1, fmt.Sprintln(text...))
}

// Warning adds a line of text to the log with a severity level 2.
func Warning(text ...interface{}) {
	std.log(2, fmt.Sprintln(text...))
}

// Error adds a line of text to the log with a severity level 3.
func Error(text ...interface{}) {
	std.log(3, fmt.Sprintln(text...))
}

// Fatal adds a line of text to the log with a severity level 4.
func Fatal(text ...interface{}) {
	std.log(4, fmt.Sprintln(text...))
}

// Default returns the standard logger used by the package-level output functions, and removes the prefixes from the log of the go standerd library.
func Default() *Logger {
	log.SetFlags(0) // Removes prefixes.
	return std
}

// Logger represents an active logging object that generates lines of output to a file by default, but can be connected to a log server.
type Logger struct {
	applicationName string
	logfilePath     string
	serverHostname  string
	serverPort      int
}

// Connect connects to the log server at {host:"", port:9999}, if a connection cannot be made it returns false and gracefully starts logging to file instead.
func (l *Logger) Connect() bool {
	return l.ConnectTo("", 9999)
}

// ConnectTo connects to the log server at host:port, if a connection cannot be made it returns false and gracefully starts logging to file instead.
func (l *Logger) ConnectTo(host string, port int) bool {
	uri := "http://" + host + ":" + strconv.Itoa(port) + "/"
	response, err := http.Get(uri)
	if err != nil {
		return false
	}
	if response.StatusCode != 200 {
		return false
	}
	l.serverHostname = host
	l.serverPort = port
	return true
}

// Write implements io.Writer so Logger can be used as an output target for log in the go standard library. Because the log in the go standerd library does not have Warning and Error methods, Write look for whether p is prefixed with either "WARNING" or "ERROR" and calls those methods on Logger removing the prefix, and otherwise calls Print.
func (l *Logger) Write(p []byte) (n int, err error) {
	message := string(p)
	if strings.HasPrefix(message, "WARNING") {
		l.Warning(strings.TrimPrefix(message, "WARNING"))
	} else if strings.HasPrefix(message, "ERROR") {
		l.Error(strings.TrimPrefix(message, "WARNING"))
	} else {
		l.Print(message)
	}
	return len(message), nil
}

// Print adds a line of text to the log with a severity level 1.
func (l *Logger) Print(text ...interface{}) {
	l.log(1, fmt.Sprintln(text...))
}

// Warning adds a line of text to the log with a severity level 2.
func (l *Logger) Warning(text ...interface{}) {
	l.log(2, fmt.Sprintln(text...))
}

// Error adds a line of text to the log with a severity level 3.
func (l *Logger) Error(text ...interface{}) {
	l.log(3, fmt.Sprintln(text...))
}

// Fatal adds a line of text to the log with a severity level 4.
func (l *Logger) Fatal(text ...interface{}) {
	l.log(4, fmt.Sprintln(text...))
}

func (l *Logger) log(severity int, message string) {
	message = strings.Trim(message, " \n")
	if len(message) == 0 {
		return
	}
	if severity >= 3 { // Higher or equal to 3 = Error and Fatal.
		message = getTrace() + message
	}
	if l.serverPort > 0 {
		err := l.logToServer(severity, message)
		if err != nil {
			l.logToFile(err.Error())
		}
		return
	}
	l.logToFile(message)
}

func (l *Logger) logToServer(severity int, message string) error {
	uri := "http://" + l.serverHostname + ":" + strconv.Itoa(l.serverPort) + "/add/"
	values := url.Values{}
	values["application"] = []string{l.applicationName}
	values["severity"] = []string{strconv.Itoa(severity)}
	values["message"] = []string{message}
	response, err := http.PostForm(uri, values)
	if err != nil {
		return err
	}
	if response.StatusCode != http.StatusCreated {
		return errors.New("HTTP Status:" + response.Status)
	}
	return nil
}

func (l *Logger) logToFile(message string) {
	logFile, err := os.OpenFile(l.logfilePath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer logFile.Close()
	message = time.Now().Format(time.RFC3339) + " " + message + "\n"
	if _, err = logFile.WriteString(message); err != nil {
		panic(err)
	}
}

func getTrace() string {
	skipCalls := 4 // 4 functions calls back up: getTrace() -> log() -> [Warning()/Error()/Fatal()] -> `actual caller`().
	pc := make([]uintptr, 15)
	n := runtime.Callers(skipCalls, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return filepath.Base(frame.File) + "[" + strconv.Itoa(frame.Line) + "] " + frame.Function + "() "
}
